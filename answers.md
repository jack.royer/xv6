# Priority queue

## 1.1

### Where do processes need to be added to the queue?

`userinit()`
`fork()`

### Where do processes need to be removed from the queue?

`freeproc()`

## 1.2

```c
void userinit(void)
{
  struct proc *p;

  // aquire the priority lock
  acquire(&prio_lock);

  p = allocproc();
  initproc = p;

  // add to the priority queue
  insert_into_prio_queue(p);

  // allocate one user page and copy init's instructions
  // and data into it.
  uvminit(p->pagetable, initcode, sizeof(initcode));
  p->sz = PGSIZE;

  // prepare for the very first "return" from kernel to user.
  p->tf->epc = 0;     // user program counter
  p->tf->sp = PGSIZE; // user stack pointer

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cmd = strdup("init");
  p->cwd = namei("/");
  p->state = RUNNABLE;

  release(&p->lock);
  release(&prio_lock);
}
```

```c
int fork(void)
{
  int i, pid;
  struct proc *np;
  struct proc *p = myproc();

  // Aquire the priority lock.
  acquire(&prio_lock);

  // Allocate process.
  if ((np = allocproc()) == 0)
  {
    return -1;
  }

  // Adds the new process to the priority queue.
  np->priority = p->priority;
  insert_into_prio_queue(np);

  // Copy user memory from parent to child.
  if (uvmcopy(p->pagetable, np->pagetable, p->sz) < 0)
  {
    freeproc(np);
    release(&np->lock);
    release(&prio_lock);
    return -1;
  }

  np->sz = p->sz;

  np->parent = p;

  // copy saved user registers.
  *(np->tf) = *(p->tf);

  // Cause fork to return 0 in the child.
  np->tf->a0 = 0;

  // increment reference counts on open file descriptors.
  for (i = 0; i < NOFILE; i++)
    if (p->ofile[i])
      np->ofile[i] = filedup(p->ofile[i]);
  np->cwd = idup(p->cwd);

  safestrcpy(np->name, p->name, sizeof(p->name));
  np->cmd = strdup(p->cmd);
  pid = np->pid;

  np->state = RUNNABLE;

  release(&np->lock);
  release(&prio_lock);

  return pid;
}
```

```c
static void
freeproc(struct proc *p)
{
  if (p->tf)
    kfree((void *)p->tf);
  p->tf = 0;
  if (p->pagetable)
    proc_freepagetable(p->pagetable, p->sz);
  if (p->cmd)
    bd_free(p->cmd);
  p->cmd = 0;
  p->pagetable = 0;
  p->sz = 0;
  p->pid = 0;
  p->parent = 0;
  p->name[0] = 0;
  p->chan = 0;
  p->killed = 0;
  p->xstate = 0;
  p->state = UNUSED;

  printf("\a");

  remove_from_prio_queue(p);
  p->priority = 0;
}
```
